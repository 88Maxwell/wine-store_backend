import mongoose from "mongoose";
import config from "../configs/db.json";
import mongooseUtils from "./utils/models";
import schemas from "./models";

const dbPort = config.port;
const dbHost = process.env.DB_HOST_TEST ? process.env.DB_HOST_TEST : config.host;
const dbName = process.env.MODE === "test" ? config.testName : config.name;

mongoose.Model.findById = mongooseUtils.methods.findById;
mongoose.Model.prototype.updateDoc = mongooseUtils.methods.updateDoc;

mongooseUtils.init(schemas, mongoose);

mongoose.connect(`mongodb://${dbHost}:${dbPort}/${dbName}`, {
    useNewUrlParser : true,
    useCreateIndex  : true
});

export default mongoose;

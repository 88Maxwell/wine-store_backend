import { Service } from "koa-service-layer";
import mongoose from "../../mongoose";
import { wine as wineDump } from "../../utils/services/dumps";

const Wine = mongoose.model("Wine");

export default class Create extends Service {
    static validate = {
        data : [ "required", { "nested_object" : {
            name        : [ "required", "string" ],
            varietal    : [ "required", "string" ],
            country     : [ "required", "string" ],
            volume      : [ "required", "integer" ],
            isImport    : [ "required" ],
            age         : [ "integer" ],
            color       : [ "string" ],
            regions     : [ "string" ],
            description : [ "string" ]
        } } ]
    };

    async execute({ data }) {
        const wine = await Wine.create(data);

        return { ...wineDump(wine) };
    }
}

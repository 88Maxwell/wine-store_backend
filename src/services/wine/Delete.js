import { Service } from "koa-service-layer";
import mongoose from "../../mongoose";
import { wine as wineDump } from "../../utils/services/dumps";

const Wine = mongoose.model("Wine");

export default class Create extends Service {
    static validate = {
        id : [ "required", "string" ]
    };

    async execute(args) {
        const wine = await Wine.findById(args.id);

        await wine.remove();

        return { ...wineDump(wine) };
    }
}

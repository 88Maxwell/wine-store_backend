import { Service } from "koa-service-layer";
import mongoose from "../../mongoose";
import { wine as wineDump } from "../../utils/services/dumps";

const Wine = mongoose.model("Wine");

export default class Create extends Service {
    static validate = {
        data : [ { "nested_object" : {
            searchValue        : [ "string" ],
            varietalFilterList : [ { "list_of": "string" } ],
            countryFilterList  : [ { "list_of": "string" } ],
            ageFilterList      : [ { "list_of": "string" } ]
        } } ]
    };

    async execute({ data }) {
        const { searchValue, varietalFilterList, countryFilterList, ageFilterList } = data;

        const query = queryCreate({
            age      : ageFilterList,
            country  : countryFilterList,
            varietal : varietalFilterList,
            name     : searchValue
        }, [
            { $in: ageFilterList },
            { $in: countryFilterList },
            { $in: varietalFilterList },
            { $regex: `.*?${searchValue}.*?` }
        ]);

        const wines = await Wine.find(query);

        return [ ...wines.map(wineDump) ];
    }
}

function queryCreate(obj, valueList) {
    const query = {};
    const keys = Object.keys(obj);

    keys.forEach((key, index) => {
        const isArray =  Array.isArray(obj[key]);
        const isObject = obj[key] instanceof Object && obj[key].constructor === Object;

        const isNotEmptyArray = isArray && obj[key].length;
        const isNotEmptyObj = isObject && Object.keys(obj[key]).length;

        const isPrimitive = !(isArray || isObject);

        if ((obj[key] && isPrimitive) || isNotEmptyArray || isNotEmptyObj) query[key] = valueList[index];
    });

    return query;
}

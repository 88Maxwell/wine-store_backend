import Router from "koa-router";
import ServiceLayer  from "../serviceLayer";
import config from "../../configs/config.json";

import WineList from "../services/wine/List";
import WineCreate from "../services/wine/Create";
import WineUpdate from "../services/wine/Update";
import WineDelete from "../services/wine/Delete";

const router = new Router({ prefix: config.PREFIX });

router.post("/wines", ServiceLayer.useService(WineList));
router.post("/wine", ServiceLayer.useService(WineCreate));
router.delete("/wine/:id", ServiceLayer.useService(WineDelete));
router.patch("/wine/:id", ServiceLayer.useService(WineUpdate));

export default router;

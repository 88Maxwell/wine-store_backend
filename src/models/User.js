import { Schema } from "mongoose";
import timestamps   from "mongoose-timestamp";
import mongooseUtils from "../utils/models";

const UserSchema = new Schema({
    email      : { type: String, required: true, index: { unique: true } },
    status     : { type: String, enum: [ "ACTIVE", "BLOCKED", "PENDING" ], default: "PENDING" },
    actionId   : { type: String, default: "" },
    firstName  : { type: String, default: "" },
    secondName : { type: String, default: "" }
});

UserSchema.plugin(mongooseUtils.plugins.passwordHash);
UserSchema.plugin(timestamps);

export default UserSchema;

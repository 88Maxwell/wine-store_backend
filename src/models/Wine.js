import mongoose   from "mongoose";
import timestamps from "mongoose-timestamp";
import uuid       from "uuid";

const Schema = mongoose.Schema;

const WineSchema = new Schema({
    _id         : { type: String, default: uuid.v4 },
    name        : { type: String, required: true },
    varietal    : { type: String, required: true, enum: [ "sweet", "semi-sweet", "dry", "semi-dry" ] },
    country     : { type: String, required: true },
    volume      : { type: Number, required: true },
    isImport    : { type: Boolean, required: true },
    age         : { type: Number, required: false },
    color       : { type: String, required: false },
    regions     : { type: String, required: false },
    description : { type: String, required: false },
    date        : { type: Date, required: true, default: new Date() }
});

WineSchema.plugin(timestamps);

export default WineSchema;

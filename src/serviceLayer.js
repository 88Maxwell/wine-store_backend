import { ServiceLayer } from "koa-service-layer";
import serviceLayerRules from "./utils/serviceLayerRules";


export default new ServiceLayer({ rules: serviceLayerRules });

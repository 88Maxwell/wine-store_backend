export function wine(obj) {
    return {
        id          : obj.id,
        name        : obj.name,
        varietal    : obj.varietal,
        country     : obj.country,
        age         : obj.age,
        volume      : obj.volume,
        isImport    : obj.isImport,
        regions     : obj.regions,
        color       : obj.color,
        description : obj.description
    };
}

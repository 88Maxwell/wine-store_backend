import validate from "./validate";
import argumentBuilder from "./argumentBuilder";

export default [
    argumentBuilder,
    validate
];


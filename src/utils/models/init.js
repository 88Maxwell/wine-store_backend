export default (schemas, mongoose) => {
    const schemaNames = Object.keys(schemas);

    schemaNames.forEach(name => mongoose.model(name, schemas[name]));
};


import crypto from "crypto";

export default {
    passwordHash : schema => {
        schema.add({
            passwordHash : { type: String, default: "" },
            salt         : { type: String, default: "" }
        });

        schema
            .virtual("password")
            .set(function setPassword(password) {
                this._password = password;
                this.salt = this.makeSalt();
                this.passwordHash = this.encryptPassword(password);
            });

        // eslint-disable-next-line
    schema.methods.checkPassword = function checkPassword(plainText) {
            return this.encryptPassword(plainText) === this.passwordHash;
        };

        // eslint-disable-next-line
    schema.methods.makeSalt = function makeSalt() {
            return `${Math.round((new Date().valueOf() * Math.random()))}`;
        };

        // eslint-disable-next-line
    schema.methods.encryptPassword = function encryptPassword(password) {
            try {
                return crypto.createHmac("sha1", this.salt).update(password).digest("hex");
            } catch (err) {
                return "";
            }
        };
    }
};

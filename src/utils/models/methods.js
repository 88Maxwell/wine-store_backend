import { Exception } from "koa-service-layer";

export default {
    async findById(id) {
        const exception = new Exception({
            code   : "WRONG_ID",
            fields : { id: "WRONG_ID" }
        });

        if (!id) throw exception;
        const doc = await this.findOne({ _id: id });

        if (!doc) throw exception;

        return doc;
    },

    async updateDoc(data) {
        const that = this;

        Object.keys(data).forEach(key => {
            that[key] = data[key];
        });

        await that.save();

        return that;
    }

};

import plugins from "./plugins";
import init from "./init";
import methods from "./methods";

export default {
    init,
    plugins,
    methods
};

import { create } from "../mocks/wine";
import mongoose from "../../src/mongoose";
import { wine as wineDump } from "../../src/utils/services/dumps";

const Wine = mongoose.model("Wine");

export async function generateWine(name = "test") {
    const wine = await Wine.create({  ...create.data, name });

    return { ...wineDump(wine) };
}

export async function generateWines(count) {
    const wineList = [];

    // eslint-disable-next-line
    for (let index = 0; index < count; index++) {
        wineList.push(await generateWine(`test ${index}`));
    }

    return wineList;
}

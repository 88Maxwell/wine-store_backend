import { request, cleanup, matchLIVR } from "../..";
import {
    listSearchUnexisted,
    searchUnexisted,
    searchTest,
    listExpect,
    searchOnlyDry,
    listSearchTestExpect
} from "../../mocks/wine";
import { emptyData } from "../../mocks";
import { generateWines } from "../../utils";

suite("List of Wines");

before(async () => {
    await generateWines(4);
});

test("Positive : List of wines", async () => {
    await request
        .post("/api/v1/wines")
        .send(emptyData)
        .expect(200)
        .expect(res => {
            matchLIVR(res.body, listExpect);
        });
});

test("Positive : Search wine", async () => {
    await request
        .post("/api/v1/wines")
        .send(searchTest)
        .expect(200)
        .expect(res => {
            matchLIVR(res.body, listSearchTestExpect);
        });
});

test("Positive : Search only dry", async () => {
    await request
        .post("/api/v1/wines")
        .send(searchOnlyDry)
        .expect(200)
        .expect(res => {
            matchLIVR(res.body, listExpect);
        });
});

test("Positive : Search nothink to find", async () => {
    await request
        .post("/api/v1/wines")
        .send(searchUnexisted)
        .expect(200)
        .expect(res => {
            matchLIVR(res.body, listSearchUnexisted);
        });
});


after(async () => {
    await cleanup();
});

import { request, cleanup, matchLIVR } from "../..";
import { update, updateExpect } from "../../mocks/wine";
import { wrongIdExpect } from "../../mocks";

import { generateWine } from "../../utils";

let wine;

suite("Update Wine");

before(async () => {
    wine = await generateWine();
});

test("Positive : Update wine ", async () => {
    await request
        .patch(`/api/v1/wine/${wine.id}`)
        .send(update)
        .expect(200)
        .expect(res => {
            matchLIVR(res.body, updateExpect);
        });
});

test("Negative : Update wine with wrong id", async () => {
    await request
        .delete("/api/v1/wine/124567890")
        .expect(200)
        .expect(res => {
            matchLIVR(res.body, wrongIdExpect);
        });
});

after(async () => {
    await cleanup();
});

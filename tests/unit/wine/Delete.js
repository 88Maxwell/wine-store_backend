import { request, cleanup, matchLIVR } from "../..";
import { createExpect } from "../../mocks/wine";
import { wrongIdExpect } from "../../mocks";
import { generateWine } from "../../utils";

let wine;

suite("Delete Wine");

before(async () => {
    wine = await generateWine();
});

test("Positive : Wine delete", async () => {
    await request
        .delete(`/api/v1/wine/${wine.id}`)
        .send({})
        .expect(200)
        .expect(res => {
            matchLIVR(res.body, createExpect);
        });
});

test("Negative : Delete wine with wrong id", async () => {
    await request
        .delete("/api/v1/wine/124567890")
        .send()
        .expect(200)
        .expect(res => matchLIVR(res.body, wrongIdExpect));
});

after(async () => {
    await cleanup();
});

import { request, cleanup, matchLIVR } from "../..";
import { create, createExpect } from "../../mocks/wine";
import { emptyDataExpect, emptyData } from "../../mocks";

suite("Create Wine");

test("Positive : Wine create", async () => {
    await request
        .post("/api/v1/wine")
        .send(create)
        .expect(200)
        .expect(res =>  matchLIVR(res.body, createExpect));
});

test("Negative : Empty data", async () => {
    await request
        .post("/api/v1/wine")
        .send(emptyData)
        .expect(200)
        .expect(res => matchLIVR(res.body, emptyDataExpect));
});

after(async () => {
    await cleanup();
});

import supertest from "supertest";
import LIVR from "livr/lib/LIVR";
import extraRules from "livr-extra-rules";
import { assert, AssertionError } from "chai";
import inspect from "util-inspect";

import serverCallback from "../src";
import mongoose from "../src/mongoose";

LIVR.Validator.registerDefaultRules(extraRules);

export const request = supertest.agent(serverCallback);

export function cleanup() {
    serverCallback.close();

    if (mongoose.connection.readyState === 1) return dropDatabase();

    return new Promise((resolve, reject) => {
        mongoose.connection.once("connected", () => {
            /* eslint-disable more/no-then */
            dropDatabase().then(resolve).catch(reject);
        });
    });
}


export function matchLIVR(got, expected) {
    try {
        const validator = new LIVR.Validator(expected);
        const validData = validator.validate(got);

        assert.ok(validData && expected, `Error: ${inspect(validator.getErrors())}`);
    } catch (err) {
        console.log("got:", got, "\n");
        console.log("expected:", expected, "\n");
        if (!(err instanceof AssertionError)) console.log(err);
        throw err;
    }
}

function dropDatabase() {
    return new Promise((resolve, reject) => {
        mongoose.connection.db.dropDatabase(err => {
            if (err) {
                return reject(err);
            }

            return resolve();
        });
    });
}

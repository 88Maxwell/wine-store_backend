export const create = {
    data : {
        name        : "test",
        varietal    : "dry",
        country     : "test",
        volume      : 14,
        isImport    : false,
        age         : 2,
        color       : "test",
        regions     : "test",
        description : "test"
    }
};

export const update = {
    data : {
        name        : "test",
        varietal    : "semi-sweet",
        country     : "test",
        volume      : 15,
        isImport    : true,
        age         : 3,
        color       : "test2",
        regions     : "test2",
        description : "test2"
    }
};

export const searchTest = {
    data : {
        searchValue : "test 3"
    }
};

export const searchOnlyDry = {
    data : {
        varietal : "dry"
    }
};

export const searchUnexisted = {
    data : {
        searchValue : "Unexisted"
    }
};


export const updateExpect = {
    status : { "is": 200 },
    data   : [ "required", { "nested_object" : {
        id          : [ "required" ],
        name        : { "is": "test" },
        varietal    : { "is": "semi-sweet" },
        country     : { "is": "test" },
        volume      : { "is": 15 },
        isImport    : { "is": true },
        age         : { "is": 3 },
        color       : { "is": "test2" },
        regions     : { "is": "test2" },
        description : { "is": "test2" }
    } } ]
};

export const createExpect = {
    status : { "is": 200 },
    data   : [ "required", { "nested_object" : {
        id          : [ "required" ],
        name        : { "is": "test" },
        varietal    : { "is": "dry" },
        country     : { "is": "test" },
        volume      : { "is": 14 },
        age         : { "is": 2 },
        isImport    : { "is": false },
        color       : { "is": "test" },
        regions     : { "is": "test" },
        description : { "is": "test" }
    } } ]
};

export const listExpect = {
    status : { "is": 200 },
    data   : [ "required", { "list_of_objects" : {
        id       : [ "required" ],
        name     : [ "required", "string" ],
        varietal : { "is": "dry" },
        country  : { "is": "test" },
        age      : { "is": 2 }
    } } ]
};

export const listSearchTestExpect = {
    status : { "is": 200 },
    data   : [ "required", { "list_of_objects" : {
        id       : [ "required" ],
        name     : { "is": "test 3" },
        varietal : { "is": "dry" },
        country  : { "is": "test" },
        age      : { "is": 2 }
    } } ]
};

export const listSearchUnexisted = {
    status : { "is": 200 },
    data   : [ "required", { "list_of_objects": {} } ]
};
